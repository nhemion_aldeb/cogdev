
var keyword_occurrences = {};
var bib_entries_by_keywords = {};
var age_list_by_publication = {};

var x_scale, y_scale;

var x_min = 0.0;
var x_max = 36.0;
var y_min = 0.0;
var y_max = NaN;

var svg_width = NaN;
var svg_height = NaN;

var plot_padding = {
    top: 50,
    bottom: 50,
    left: 50,
    right: 50
};

var plot_width = NaN;
var plot_height = NaN;

var plot_translate_str = function() {
    return "translate(" + plot_padding.left + "," + plot_padding.top + ")";
};

var color_scheme = {
    bg: "#ffffff",// "#FCFEFD",
};

    
$(document).ready(function() {
    init_demo();
});


function init_demo() {
    
    d3.select("#democontainer")
	.style("visibility", "visible");
    
    d3.select("#plotcontainer")
	.append("div")
	.attr("id", "legend");
    
    var leftmenu_div = d3.select("#plotcontainer")
	.append("div")
	.attr("id", "leftmenu");
    
    leftmenu_div
	.append("select")
	.attr("id", "keyword-select")
	.attr("multiple", "");
    
    leftmenu_div
	.append("div")
	.style("clear", "both");
    
    d3.select("#svgcontainer")
	.style("width", "610px")
	.style("height", "500px");

    $.getJSON("desc_data_short.json", function(data) {
	
	bib_entries_by_keywords = data["bib_entries_by_keywords"];
	keyword_occurrences = data["keyword_occurrences"];
	number_of_articles_by_keyword = data["number_of_articles_by_keyword"];
	
	// add keywords as options to select box
	
	$keywordselect = $('#keyword-select');
	$keywordselect.empty();

	for (var ind = 0; ind < number_of_articles_by_keyword.length; ind++) {
	    var stem = number_of_articles_by_keyword[ind][0];
	    keyword_top_occurrence = keyword_occurrences[stem];
	    $("<option></option>").attr("value", ind).attr("stem", stem).html(keyword_top_occurrence).appendTo($keywordselect);
	}
	
	$.ajax({
	    type: "GET",
	    async: true,
	    url: "study_annotations.xml",
	    dataType: "xml",
	    success: function(data) {
		
		parse_age_lists_from_annotations_xml(data);
		
		$("#keyword-select").change(keyword_select_callback);
		
		document.getElementById("keyword-select").selectedIndex = 5;
		keyword_select_callback();
		
	    }
	});
    });
}


function keyword_select_callback() {
    
    var selected_options = $("#keyword-select option:selected");
    
    var publications = {};
    var number_of_publications = 0;
    for (var k = 0; k < selected_options.length; k++) {
	var keyword = selected_options[k].getAttribute("stem");
	for (var k = 0; k < bib_entries_by_keywords[keyword].length; k++) {
	    var bib_entry = bib_entries_by_keywords[keyword][k];
	    if (typeof publications[bib_entry] == 'undefined') {
		publications[bib_entry] = true;
		number_of_publications += 1;
	    }
	}
    }
    
    // clear plot
    clear_plot();
    init_plot(number_of_publications);
    
    var color_scale = d3.scale.category10();
    
    var keywords = [];
    var colors = [];
    for (var idx = 0; idx < selected_options.length; idx++) {
	keywords.push(selected_options[idx].getAttribute("stem"));
	colors.push(color_scale(idx % 10));
    }
    
    plot_distributions(keywords, colors);
    plot_age_list(keywords, colors);
    add_legend_entry(keywords, colors);
    
}


function parse_age_lists_from_annotations_xml(xml) {
    
    var $publications = $(xml).find('publication');
    
    age_list_by_publication = {}

    for (var j = 0; j < $publications.length; j++) {
	var $publication = $publications.eq(j);
	
	var id = parseInt($publication.attr('id'));
	
	age_list_by_publication[id] = [];
	
	var $groups = $publication.find('group');
	
	for (var k = 0; k < $groups.length; k++) {
	    var $group = $groups.eq(k);

	    var group_age_range = [];
	    
	    // does this group entry have a distribution entry?
	    $distributions = $group.find('distribution');
	    if ($distributions.length) {

		var min_age = NaN;
		var max_age = NaN;
		
		// parse distribution
		for (var l = 0; l < $distributions.length; l++) {
		    var $distribution = $distributions.eq(l);
		    
		    var min_age = compute_months($distribution.find('min'));
		    var max_age = compute_months($distribution.find('max'));
		    var mean_age = compute_months($distribution.find('mean'));
		    var std_age = compute_months($distribution.find('std'));
		    
		    if (isNaN(std_age)) {
			// std_age could not be computed -- use default value
			std_age = 1.0;
		    }
		    if (isNaN(min_age) || isNaN(max_age)) {
			min_age = mean_age - 2.0 * std_age;
			max_age = mean_age + 2.0 * std_age;
		    }
		    
		    group_age_range = [min_age, max_age];
		    
		}
		
	    } else { 
		
		// parse visits
		$visits = $group.find('visit');
		
		var visit_age_list = [];
		
		for (var l = 0; l < $visits.length; l++) {
		    $visit = $visits.eq(l);
		    
		    visit_age_list.push(compute_months($visit));
		}
		
		if (visit_age_list.length == 1) {
		    // only a single visit (age range is only a point,
		    // not an interval) -- use small interval around
		    // the visit to represent it
		    var std_age = 3.0;
		    group_age_range = [visit_age_list[0] - std_age, 
				       visit_age_list[0] + std_age];
		} else {
		    group_age_range = visit_age_list;
		}
		
	    }
	    
	    age_list_by_publication[id].push(group_age_range);
	}
    }
}

function compute_months($element) {
    
    var age_in_months = NaN;
    
    if ($element.length) {
	// read values, default to 0 if no entry exists
	var y = (typeof (val = $element.attr('y')) == 'undefined' ) ? 0 : parseFloat(val);
	var m = (typeof (val = $element.attr('m')) == 'undefined' ) ? 0 : parseFloat(val);
	var w = (typeof (val = $element.attr('w')) == 'undefined' ) ? 0 : parseFloat(val);
	var d = (typeof (val = $element.attr('d')) == 'undefined' ) ? 0 : parseFloat(val);
	
	// compute age in months
	age_in_months = 12.0 * y + m + 7.0 * w / 30.0 + d / 30.0 ;
    }
    
    return age_in_months;
    
}


function init_plot(number_of_articles) {
    
    var svg_div = d3.select("#svgcontainer");
    var svg = d3.select("#plotsvg");
    
    svg_width = parseInt(svg_div.style("width"));
    svg_height = parseInt(svg_div.style("height"));
    plot_width = svg_width - plot_padding.left - plot_padding.right;
    plot_height = svg_height - plot_padding.top - plot_padding.bottom;
    
    x_scale = d3.scale.linear()
	.range([0, plot_width])
	.domain([x_min, x_max]);
    
    svg.attr("width", svg_width)
	.attr("height", svg_height);
    
    var canvas = svg.append("g")
	.attr("id", "canvas")
        .attr("transform", "translate(" + plot_padding.left + ", " + (plot_height + plot_padding.top) + ")");
    
    // define line marker
    canvas.append("defs")
	.append("marker")
	.attr("id", "age-line-marker")
	.attr("viewBox", "0 0 8 8")
	.attr("refX", "4")
	.attr("refY", "4")
	.attr("markerWidth", "4")
	.attr("markerHeight", "4")
	.attr("stroke", "none")
	.attr("fill", "#000000")
	.attr("orient", "auto")
	.append("circle")
	.attr("id", "marker-circle")
	.attr("cx", "4")
	.attr("cy", "4")
	.attr("r", "2");
    
}

function draw_axes() {
    
    var axis_ticks_size = 7;
    
    var canvas = d3.select("#canvas");
    
    // draw a grid and an axis
    
    var x_axis = d3.svg.axis()
	.scale(x_scale)
	.orient("bottom")
	.tickSize(y_scale(y_max));
    var y_axis = d3.svg.axis()
	.scale(y_scale)
	.orient("left")
	.tickSize(-x_scale(x_max));
    
    // grid
    canvas.append("g")
     	.attr("class", "grid")
    	.attr("id", "plot-grid")
    	.call(x_axis.ticks(36))
	.selectAll(".tick")
	.data(x_scale.ticks(5), function(d) {return d;})
	.exit()
	.classed("minor", true);
    canvas.append("g")
     	.attr("class", "grid")
    	.attr("id", "plot-grid")
    	.call(y_axis);
    // axis
    canvas.append("g")
    	.attr("class", "axis")
    	.attr("id", "x-axis")
	.call(x_axis.ticks(8).tickSize(axis_ticks_size));
    canvas.append("g")
    	.attr("class", "axis")
    	.attr("id", "y-axis")
	.call(y_axis.ticks(5).tickSize(axis_ticks_size));
    
    axis_label_font_size = 15;
    
    label_distance_to_axis = 2.5 * axis_label_font_size + axis_ticks_size;

    canvas.append("text")
	.attr("x", plot_width)
	.attr("y", label_distance_to_axis)
	.style("font-size", axis_label_font_size + "px")
	.style("text-anchor", "end")
	.text("age in months");
    
    canvas.append("g")
	.attr("transform", "translate(" + (-label_distance_to_axis+axis_label_font_size) + "," + (-plot_height) + ")")
	.append("g")
	.attr("transform", "rotate(270)")
	.append("text")
	.attr("x", 0)
	.attr("y", 0)
	.style("font-size", axis_label_font_size + "px")
	.style("text-anchor", "end")
	.text("number of articles");
    
}

function plot_age_list(keywords, colors) {
    
    var canvas = d3.select("#canvas");
    
    // compute total number of articles for all selected keywords
    total_nb_of_articles = 0;
    for (var idx = 0; idx < keywords.length; idx++) {
	total_nb_of_articles += bib_entries_by_keywords[keywords[idx]].length;
    }
    
    y_scale = d3.scale.linear()
	.range([0, -plot_height])
	.domain([-1, total_nb_of_articles]);
    
    // draw rectangles
    article_counter = -1;
    for (var kw_idx = 0; kw_idx < keywords.length; kw_idx++) {
	keyword = keywords[kw_idx];
	color = colors[kw_idx];
	for (var pub_idx = 0; pub_idx < bib_entries_by_keywords[keyword].length; pub_idx++) {
	    
	    article_counter += 1;
	    
	    id = bib_entries_by_keywords[keyword][pub_idx]
	    
	    var age_intervals = age_list_by_publication[id].map(function(d) {
		return [d3.min(d), d3.min([d3.max(d), x_max])];
	    });
	    
	    var g = canvas.append("g")
		.attr("class", "age-intervals-visualization");
	    
	    g.selectAll("rect")
		.data(age_intervals)
		.enter()
		.insert("rect")
		.attr("x", function(d) {return x_scale(d[0]);})
		.attr("y", -plot_height)
		.attr("width", function(d) {return x_scale(d[1]) - x_scale(d[0]);})
		.attr("height", plot_height)
		.attr("class", "age-rect")
		.style("fill", color);
	    
	    for (var j = 0; j < age_list_by_publication[id].length; j++) {
		var publication_age_list = age_list_by_publication[id][j];
		
		var line = d3.svg.line()
		    .x(function(d) {return x_scale(d3.min([d, x_max]));})
		    .y(function(d) {return y_scale(article_counter);})
		    .interpolate("linear");
		
		var g = canvas.append("g")
		    .attr("class", "age-intervals-visualization");
		
		g.insert("path")
		    .attr("d", line(publication_age_list))
		    .attr("marker-start", "url(#age-line-marker)")
		    .attr("marker-end", "url(#age-line-marker)")
		    .attr("marker-mid", "url(#age-line-marker)")
		    .attr("stroke-width", "2px")
		    .attr("fill", "none")
		    .attr("stroke", color)
		    .style("opacity", "0.0");
	    }
	}
    }
    
    var paths = d3.select("#plotsvg").selectAll("path");
    var rects = d3.select("#plotsvg").selectAll("rect");
    
    paths.transition().duration(1000).style("opacity", 1.0);
    rects.transition().duration(1000).style("opacity", 1.0 / rects[0].length);

}


function convolve(x, m) {
    // initialize result array y of same length as input x

    var y = [];
    x.forEach(function() {y.push();});
    
    // compute convolution
    m_offset = parseInt(m.length / 2.0);
    for (var u = 0; u < x.length; u++) {
	var val = 0;
	for (var v = 0; v < m.length; v++) {
	    // compute index in x
	    var k = u + v - m_offset;
	    k = k < 0 ? NaN : k > x.length-1 ? NaN : k;
	    val += isNaN(k) ? 0 : x[k] * m[v];
	}
	y[u] = val;
    }
    
    return y;
}

function plot_distributions(keywords, colors) {
    
    var canvas = d3.select('#canvas');
    
    // create array of values along time axis ("pixels")
    var x_data = [];
    var y_data = [];
    var resolution = 72;
    var d_x = (x_scale.domain()[1] - x_scale.domain()[0]) / resolution;
    for (var t = 0; t <= x_max; t += d_x) {
	x_data.push(parseFloat(t));
	y_data.push(parseFloat(0));
    }
    
    var y_data_list = [];
    
    // define convolution mask
    var gaussian_mask = function(n) {
    	var a = 5;
    	var x = d3.range(-1, 1+2/(n-1), 2/(n-1));
    	x = x.map(function(d) {return Math.exp(-a*d*d);});
    	var sum_x = d3.sum(x);
    	x = x.map(function(d) {return d / sum_x;});
    	return x;
    }
    var mask = gaussian_mask(15);
    
    for (var kw_idx = 0; kw_idx < keywords.length; kw_idx++) {
	keyword = keywords[kw_idx];
	color = colors[kw_idx];
	
	y_data = y_data.map(function() {return 0;});
	
	for (var pub_idx = 0; pub_idx < bib_entries_by_keywords[keyword].length; pub_idx++) {
	    
	    id = bib_entries_by_keywords[keyword][pub_idx]
	    
	    var x_is_in_age_lists = age_list_by_publication[id].map(function(d) {
		min = d3.min(d);
		max = d3.max(d);
		return x_data.map(
		    function(x_d) {
			return x_d >= min && x_d <= max;
		    });
	    });
	    
	    for (var age_list_idx = 1; age_list_idx < x_is_in_age_lists.length; age_list_idx++) {
		x_is_in_age_lists[0] = d3.zip(x_is_in_age_lists[0],
					      x_is_in_age_lists[age_list_idx])
		    .map(function(d) {
			return d[0] | d[1];
		    });
	    }
	    x_is_in_age_lists = x_is_in_age_lists[0];
	    
	    y_data = d3.zip(y_data, x_is_in_age_lists).map(function(d) {
		return d[0] + d[1];
	    });
	    
	}
	
	y_data = convolve(y_data, mask);
	
	y_data_list.push(y_data);

    }
    
    y_max = d3.max(y_data_list.map(function(d) {return d3.max(d);})) + 0.5;
    
    y_scale = d3.scale.linear()
	.range([0, -plot_height])
	.domain([y_min, y_max]);
    
    draw_axes();
    
    plot_line = d3.svg.line()
    	.x(function(d) {return x_scale(d[0]);})
    	.y(function(d) {return y_scale(d[1])})
    	.interpolate("basis");
    
    for (var idx = 0; idx < y_data_list.length; idx++) {
	var g = canvas.append("g")
    	    .attr("hold", "false")
    	    .attr("class", "age-distribution-visualization");
	
	g.append("path")
    	    .attr("d", plot_line(d3.zip(x_data, y_data_list[idx])))
    	    .attr("class", "age-distribution")
	    .style("stroke", colors[idx])
	    .transition().duration(1000).style("opacity", 1.0);
    }
	

}


function clear_plot() {
    
    d3.select("#canvas").remove();
    clear_legend();
    
}


function add_legend_entry(keywords, colors) {
    
    for (var idx = 0; idx < keywords.length; idx++) {
	
	var entry = d3.select("#legend")
	    .append("div")
	    .attr("class", "legend-entry");
	
	keyword_top_occurrence = keyword_occurrences[keywords[idx]];
	
	entry.append("div")
	    .attr("class", "legend-entry-line")
	    .style("background", colors[idx])
	    .style("padding", "0px")
	    .style("margin-right", "10px");
	
	entry.append("div")
	    .attr("class", "legend-entry-label")
	    .text(keyword_top_occurrence)
	    .style("color", colors[idx])
	    .style("padding", "0px")
	    .style("margin", "0px");
	
	entry.append("div")
	    .style("clear", "both")
	    .style("padding", "0px");
    }
    
}    


function clear_legend() {
    d3.select('#legend').selectAll('.legend-entry').remove();
}
