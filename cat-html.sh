#!/bin/bash

for file in _*.html
do
    outname=$(echo $file | sed 's/_\(.*\)/\1/')
    cat header-1.html header-2.html $file footer.html > $outname
done
